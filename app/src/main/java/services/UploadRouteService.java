package services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UploadRouteService extends Service {

    public static final String ROUTE_NAME = "route_name";
    public static final String LAT_POSITION = "lat_position";
    public static final String LONG_POSITION = "long_position";
    public  static  final  String TAG_ROUTES = "app_routes";
    DatabaseReference db;

    public UploadRouteService() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        db = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG_ROUTES, "is created");
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (intent != null) {
                    Log.i(TAG_ROUTES, "INIT");
                    String name = intent.getStringExtra(ROUTE_NAME);
                    String lat_p = intent.getStringExtra(LAT_POSITION);
                    String long_p = intent.getStringExtra(LONG_POSITION);

                    DatabaseReference dbTemp = db.child(name).push();
                    dbTemp.child(LAT_POSITION).setValue(lat_p);
                    dbTemp.child(LONG_POSITION).setValue(long_p);

                    try {
                        Thread.sleep(20000);
                    }catch (InterruptedException ie){
                    }

                    Log.i(TAG_ROUTES, "FINISH");
                }
            }
        }).start();
        return Service.START_STICKY;
    }

    @Override
     public void onDestroy() {
        super.onDestroy();
        Log.i(TAG_ROUTES, "is destroyed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
